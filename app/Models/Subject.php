<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Subject
 *
 * @package App
 * @property string $name
*/
class Subject extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public static function storeValidation($request)
    {
        return [
            'name' => 'max:128|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'name' => 'max:128|required'
        ];
    }
}
