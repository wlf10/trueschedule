<!DOCTYPE html>
<html lang="en">
    <head>
        @include('components.head')
    </head>
    
    <body class="hold-transition sidebar-mini">
        <div class="wrapper" id="app">
            @include('components.navbar')
            @include('components.sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <div class="content">
                    <div class="container-fluid">
                        <router-view></router-view>
                        <vue-progress-bar></vue-progress-bar>
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            @include('components.footer')
        </div>
        <!-- ./wrapper -->
    </body>
</html>